# README

This project uses Rails as Backend and React as Frontend.
To install this project you have to follow the next steps:

## Make migrations

```console
foo@bar:~$ rails db:migrate
```

## Install gems with bundle

```console
foo@bar:~$ bundle install
```

## Install front packages with npm

```console
foo@bar:~$ npm install
```

## Install front packages with yarn

```console
foo@bar:~$ yarn install
```

## Run server

```console
foo@bar:~$ rails s
```
