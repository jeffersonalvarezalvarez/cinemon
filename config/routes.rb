Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :v1, defaults: { format: 'json' } do
    # Films
    get 'films', to: 'films#index'
    get 'films/:id', to: 'films#show'
    post 'films', to: 'films#create'

    # Bookings
    get 'bookings', to: 'bookings#index'
    get 'bookings/:id', to: 'bookings#show'
    post 'bookings', to: 'bookings#create'
  end

  # Forward all requests to static controller index
  get '*page', to: 'static#index', constraints: ->(req) do
    !req.xhr? && req.format.html?
  end
  # Root page
  root 'static#index'
end
