import { createStore, applyMiddleware } from "redux";

import thunk from "redux-thunk";

import * as constantsBooking from "../constants/Booking";

const getInitialState = (csrf_token) => {
  return {
    bookings    : [],
    csrf_token  : csrf_token
  }
};

function rootReducer(state, action) {
  switch(action.type) {
    case constantsBooking.GET_BOOKINGS_SUCCESS:
      return {
        bookings   : action.json.bookings,
        csrf_token : state.csrf_token
      };
    default:
      return state;
  }
}

export default function configureStore(csrf_token) {
  const store = createStore(
    rootReducer,
    getInitialState(csrf_token),
    applyMiddleware(
      thunk
    )
  );
  return store;
};
