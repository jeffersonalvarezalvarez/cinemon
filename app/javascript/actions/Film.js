import { createStore, applyMiddleware } from "redux";

import thunk from "redux-thunk";

import * as constantsFilm from "../constants/Film";

const getInitialState = (csrf_token) => {
  return {
    films       : [],
    csrf_token  : csrf_token
  }
};

function rootReducer(state, action) {
  switch(action.type) {
    case constantsFilm.GET_FILMS_SUCCESS:
      return {
        films      : action.json.films,
        csrf_token : state.csrf_token
      };
    default:
      return state;
  }
}

export default function configureStore(csrf_token) {
  const store = createStore(
    rootReducer,
    getInitialState(csrf_token),
    applyMiddleware(
      thunk
    )
  );
  return store;
};
