import React from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

import FilmForm from "./FilmNew";
import BookingForm from "../bookings/BookingNew";

import * as constantsFilm from "../../constants/Film";

function getFilms(date) {
  return dispatch => {
    return fetch(`http://localhost:3000/v1/films?date=${date}`)
      .then(response => {
        if(response.status !== 200) {
          throw new Error("Error mientras se obtenían películas.");
        }
        return response.json(); })
      .then(json => dispatch(getFilmsSuccess(json)))
      .catch(() => {
        toastr.error("Error obteniendo películas. Intenta de nuevo.");
      });
  };
};

function addFilm(event, data, csrf_token) {
  event.preventDefault();
  
  const fmtData = {
    film: {
      ...data
    }
  };

  return dispatch => {
    return fetch(`http://localhost:3000/v1/films`, {
        method: "POST",
        body: JSON.stringify(fmtData),
        headers:{
          "Content-Type": "application/json",
          "X-CSRF-Token": csrf_token
        } })
      .then(response => {
        if(response.status !== 201) {
          throw new Error("Error guardando la película.");
        }
        return true; })
      .then(() => fetch(`http://localhost:3000/v1/films`))
      .then(response => {
        if(response.status !== 200) {
          throw new Error("Error mientras se obtenían películas.");
        }
        toastr.success("Película creada correctamente.");
        $('#addFilm').modal('toggle');
        return response.json(); })
      .then(json => dispatch(getFilmsSuccess(json)))
      .catch(() => {
        toastr.error("Error creando la película. Intenta de nuevo.");
      });
  };
};

function addBooking(event, data, csrf_token) {
  event.preventDefault();
  
  const fmtData = {
    booking: {
      ...data
    }
  };

  return dispatch => {
    return fetch(`http://localhost:3000/v1/bookings`, {
        method: "POST",
        body: JSON.stringify(fmtData),
        headers:{
          "Content-Type": "application/json",
          "X-CSRF-Token": csrf_token
        } })
      .then(response => {
        if(response.status !== 201) {
          throw new Error("Error guardando la reserva.");
        }
        toastr.success("Reserva creada correctamente.");
        $('#addBooking'+fmtData.booking.film_id).modal('toggle');
        return true; })
      .catch(() => {
        toastr.error("No hay más sillas disponibles para esta fecha.");
      });
  };
};

export function getFilmsSuccess(json) {
  return {
    type: constantsFilm.GET_FILMS_SUCCESS,
    json
  };
};

class FilmsList extends React.Component {
  componentDidMount() {
    this.props.getFilms("");
  };

  getFilmsList(films) {
    if(films && films.length === 0) {
      return <div className="col-md-12 text-center">
        <img src="./assets/file-empty-icon.png" height="200" width="200" />
        <p>Ups! No hay películas disponibles</p>
      </div>
    }
    return films && films.map(film => {
      return <div
        key={film.id}
        className="col-md-4 p-3">
        <div className="card">
          <img src={film.image} className="card-img-top" />
          <div className="card-body">
            <h5 className="card-title">{film.name}</h5>
            <p className="card-text">{film.description}</p>
            <button
              className="btn btn-primary"
              data-toggle="modal"
              data-target={"#addBooking"+film.id}>
              Reservar
            </button>
            <div
              className="modal fade"
              id={"addBooking"+film.id}
              tabIndex="-1"
              role="dialog"
              aria-labelledby="addBookingLabel"
              aria-hidden="true">
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5
                      className="modal-title"
                      id="addBookingLabel">
                      Hacer reserva
                    </h5>
                    <button
                      type="button"
                      className="close"
                      data-dismiss="modal"
                      aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <BookingForm
                      csrf_token={this.props.csrf_token}
                      addBooking={this.props.addBooking}
                      film_id={film.id}
                    />
                  </div>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-secondary"
                      data-dismiss="modal">
                      Cerrar
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    });
  };

  render() {
    const { films } = this.props;
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-md-4">
            <h3>Películas en cartelera</h3>
          </div>
          <div className="col-md-4">
            <form onSubmit={this.onSubmit}>
              <div className="row">
                <div className="col-md-3">
                  <label htmlFor="date">
                    Fecha
                  </label>
                </div>
                <div className="col-md-9">
                  <input
                    type="date"
                    id="date"
                    className="form-control"
                    onChange={(event) => this.props.getFilms(event.target.value)}
                  />
                </div>
              </div>
            </form>
          </div>
          <div className="col-md-4">
            <div className="float-right">
              <button
                className="btn btn-primary"
                data-toggle="modal"
                data-target="#addFilm">
                Nueva película
              </button>
              <div
                className="modal fade"
                id="addFilm"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="addFilmLabel"
                aria-hidden="true">
                <div className="modal-dialog" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5
                        className="modal-title"
                        id="addFilmLabel">
                        Crear película
                      </h5>
                      <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      <FilmForm
                        csrf_token={this.props.csrf_token}
                        addFilm={this.props.addFilm}
                      />
                    </div>
                    <div className="modal-footer">
                      <button
                        type="button"
                        className="btn btn-secondary"
                        data-dismiss="modal">
                        Cerrar
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br />
        <br />
        <div className="row">
          {this.getFilmsList(films)}
        </div>
      </React.Fragment>
    );
  };
};

const structuredSelector = createStructuredSelector({
  films      : state => state.films,
  csrf_token : state => state.csrf_token
});

const mapDispatchToProps = { getFilms, addFilm, addBooking };

export default connect(structuredSelector, mapDispatchToProps)(FilmsList);
