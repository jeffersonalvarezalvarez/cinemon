import React from "react";

class FilmForm extends React.Component {
  constructor(props) {
    super(props);

    this.daysPicker = React.createRef();
    this.state = {
      name        : "",
      description : "",
      image       : "",
      days        : ""
    };
  };

  componentDidMount() {
    $("#days").datepicker({
      multidate: true,
      format: "yyyy-mm-dd"
    });
    let _this = this;
    $("#days").on("changeDate", function(e) {
      _this.onChange(e);
    });
  };

  onChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  render() {
    return (
      <form onSubmit={(event) =>
        this.props.addFilm(event, this.state, this.props.csrf_token)}>
        <div className="form-group row">
          <div className="col-md-2">
            <label htmlFor="name">
              Nombre
            </label>
          </div>
          <div className="col-md-10">
            <input
              name="name"
              id="name"
              className="form-control"
              onChange={this.onChange}
              placeholder="Nombre"
              required
            />
          </div>
        </div>
        <div className="form-group row">
          <div className="col-md-2">
            <label htmlFor="description">
              Desc
            </label>
          </div>
          <div className="col-md-10">
            <input
              name="description"
              id="description"
              className="form-control"
              onChange={this.onChange}
              placeholder="Descripción"
              required
            />
          </div>
        </div>
        <div className="form-group row">
          <div className="col-md-2">
            <label htmlFor="image">
              URL de la imagen
            </label>
          </div>
          <div className="col-md-10">
            <input
              name="image"
              id="image"
              className="form-control"
              type="url"
              onChange={this.onChange}
              placeholder="Imagen"
              required/>
          </div>
        </div>
        <div className="form-group row">
          <div className="col-md-2">
            <label htmlFor="days">
              Días
            </label>
          </div>
          <div className="col-md-10">
            <input
              ref={this.daysPicker}
              name="days"
              id="days"
              className="form-control"
              placeholder="Días"
              required
            />
          </div>
        </div>
        <div className="row text-center">
          <div className="col-md-12">
            <button
              type="submit"
              className="btn btn-primary">
              Guardar
            </button>
          </div>
        </div>
      </form>
    );
  };
};

export default FilmForm;
