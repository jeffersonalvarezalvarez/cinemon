import React from "react";

import { BrowserRouter, Switch, Route } from "react-router-dom";

import { Provider } from "react-redux";

// Navigation
import Navigation from "./navigation/Navigation";

// Films
import FilmList from "./films/FilmList";
import BookingList from "./bookings/BookingList";

// Stores
import configGlobalStore from "../actions/Global";
import configFilmStore from "../actions/Film";
import configBookingStore from "../actions/Booking";

// Contexts
const GlobalContext = React.createContext();

class App extends React.Component {
  render () {
    return (
      <BrowserRouter>
        <Navigation />
        <div className="container mb-5">
          <Switch>
            <Provider context={GlobalContext} store={configGlobalStore()}>
              <Provider store={configFilmStore(this.props.csrf_token)}>
                <Switch>
                  <Route exact path="/" component={FilmList}></Route>
                </Switch>
              </Provider>
              <Provider store={configBookingStore(this.props.csrf_token)}>
                <Switch>
                  <Route exact path="/bookings" component={BookingList}></Route>
                </Switch>
              </Provider>
            </Provider>
          </Switch>
        </div>
      </BrowserRouter>
    )
  }
}

export default App;
