import React from "react";

class BookingForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      day       : "",
      dni       : "",
      email     : "",
      cellphone : "",
      film_id   : props.film_id
    };
  };

  onChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  render() {
    return (
      <form onSubmit={(event) =>
        this.props.addBooking(event, this.state, this.props.csrf_token)}>
        <div className="form-group row">
          <div className="col-md-2">
            <label htmlFor="day">
              Día
            </label>
          </div>
          <div className="col-md-10">
            <input
              name="day"
              id="day"
              type="date"
              className="form-control"
              onChange={this.onChange}
              placeholder="Día"
              required
            />
          </div>
        </div>
        <div className="form-group row">
          <div className="col-md-2">
            <label htmlFor="dni">
              Cédula
            </label>
          </div>
          <div className="col-md-10">
            <input
              name="dni"
              id="dni"
              type="number"
              className="form-control"
              onChange={this.onChange}
              placeholder="Cédula"
              required
            />
          </div>
        </div>
        <div className="form-group row">
          <div className="col-md-2">
            <label htmlFor="email">
              Email
            </label>
          </div>
          <div className="col-md-10">
            <input
              name="email"
              id="email"
              className="form-control"
              type="email"
              onChange={this.onChange}
              placeholder="Email"
              required/>
          </div>
        </div>
        <div className="form-group row">
          <div className="col-md-2">
            <label htmlFor="cellphone">
              Celular
            </label>
          </div>
          <div className="col-md-10">
            <input
              name="cellphone"
              id="cellphone"
              type="number"
              className="form-control"
              onChange={this.onChange}
              placeholder="Celular"
              required
            />
          </div>
        </div>
        <div className="row text-center">
          <div className="col-md-12">
            <button
              type="submit"
              className="btn btn-primary">
              Reservar
            </button>
          </div>
        </div>
      </form>
    );
  };
};

export default BookingForm;
