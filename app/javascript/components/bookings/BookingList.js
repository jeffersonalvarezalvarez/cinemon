import React from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

import * as constantsBooking from "../../constants/Booking";

function getBookings() {
  return dispatch => {
    return fetch(`http://localhost:3000/v1/bookings`)
      .then(response => {
        if(response.status !== 200) {
          throw new Error("Error mientras se obtenían reservas.");
        }
        return response.json(); })
      .then(json => dispatch(getBookingsSuccess(json)))
      .catch(() => {
        toastr.error("Error obteniendo películas. Intenta de nuevo.");
      });
  };
};

export function getBookingsSuccess(json) {
  return {
    type: constantsBooking.GET_BOOKINGS_SUCCESS,
    json
  };
};

class BookingsList extends React.Component {
  componentDidMount() {
    this.props.getBookings();
  };

  getBookingsList(bookings) {
    return bookings && bookings.map(booking => {
      return <tr key={booking.id}>
        <td>{booking.film.name}</td>
        <td>{booking.day}</td>
        <td>{booking.dni}</td>
        <td>{booking.email}</td>
        <td>{booking.cellphone}</td>
      </tr>;
    });
  };

  render() {
    const { bookings } = this.props;
    return (
      <div className="row">
        <h3>Reservas</h3>
        <table className="table">
          <thead>
            <tr>
              <th>Película</th>
              <th>Día</th>
              <th>Cédula</th>
              <th>Correo</th>
              <th>Celular</th>
            </tr>
          </thead>
          <tbody>
            {this.getBookingsList(bookings)}
          </tbody>
        </table>
      </div>
    );
  };
};
  
const structuredSelector = createStructuredSelector({
  bookings   : state => state.bookings,
  csrf_token : state => state.csrf_token
});

const mapDispatchToProps = { getBookings };

export default connect(structuredSelector, mapDispatchToProps)(BookingsList);
