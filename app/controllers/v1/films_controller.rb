class V1::FilmsController < ApplicationController
  before_action :set_film, only: [:show]

  # GET /films.json
  def index
    @films = nil
    if params[:date].present?
      @films = Film.where("days like ?", "%#{params[:date]}%")
    else
      @films = Film.all
    end

    respond_to do |format|
      format.json { render json: { :films => @films } }
    end
  end

  # GET /films/:id.json
  def show
    if @film.present?
      respond_to do |format|
        format.json { render json: { :film => @film } }
      end
    else
      respond_to do |format|
        format.json { render json: { error: "Film not found" }, status: :not_found }
      end
    end
  end

  # POST /films.json
  def create
    @film = Film.new(film_params)

    respond_to do |format|
      if @film.save
        format.json { render json: @film, status: :created }
      else
        format.json { render json: @film.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_film
      @film = Film.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def film_params
      params.require(:film).permit(:name, :description, :image, :days)
    end
end
