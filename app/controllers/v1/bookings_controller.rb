class V1::BookingsController < ApplicationController
  before_action :set_booking, only: [:show]

  # GET /bookings.json
  def index
    @bookings = Booking.includes(:film)
    respond_to do |format|
      format.json { render json: { :bookings => @bookings }, :include => {:film => {:only => :name}} }
    end
  end

  # GET /bookings/:id.json
  def show
    if @booking.present?
      respond_to do |format|
        format.json { render json: { :booking => @booking } }
      end
    else
      respond_to do |format|
        format.json { render json: { error: "Booking not found" }, status: :not_found }
      end
    end
  end

  # POST /bookings.json
  def create
    @booking = Booking.new(booking_params)

    if @booking.film.bookings.where("day = ?", @booking.day).size === 10
      respond_to do |format|
        format.json { render json: { :message => "No more bookings, 10 maximum" }, status: :unprocessable_entity }
      end
    else
      respond_to do |format|
        if @booking.save
          format.json { render json: @booking, status: :created }
        else
          format.json { render json: @booking.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_booking
      @booking = Booking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def booking_params
      params.require(:booking).permit(:film_id, :day, :dni, :email, :cellphone)
    end
end
