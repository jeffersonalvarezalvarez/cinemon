class CreateBookings < ActiveRecord::Migration[5.2]
  def change
    create_table :bookings do |t|
      t.references :film, foreign_key: true
      t.string :day
      t.string :dni
      t.string :email
      t.string :cellphone

      t.timestamps
    end
  end
end
